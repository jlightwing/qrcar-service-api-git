package com.lightwing.qrcar.service.api;

import com.lightwing.qrcar.jaxb.VehicleRequest;
import com.lightwing.qrcar.jaxb.VehicleResponse;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

/**
 * An interface describing the external services provided by the Qrcar web application, with annotations used to
 * describe the path the various functions are mapped to, as well as the media type used in the request if any.
 *
 * The functions will take various requests of different media types within HTTP methods, perform the relevant internal
 * processing, and return a relevant response
 */
@Path("/QrcarExternalService")
public interface QrcarExternalService {

  /**
  * Takes VehicleRequest type xml request, and returns a VehicleResponse type based on the code within the request.
  *
  * @param request the VehicleRequest containing the code to get the VehicleResponse
  * @return the VehicleResponse based on the provided code
  */
  @Path("/getVehicleByCode")
  @POST
  @Consumes({MediaType.APPLICATION_XML })
  VehicleResponse getVehicleByCode(VehicleRequest request);
}
