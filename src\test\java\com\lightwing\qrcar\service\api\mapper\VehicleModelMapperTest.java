package com.lightwing.qrcar.service.api.mapper;

import com.lightwing.qrcar.da.api.model.Vehicle;
import com.lightwing.qrcar.jaxb.VehicleResponse;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class VehicleModelMapperTest {

  private Fixture fixture;

  public static final BigDecimal PRICE = BigDecimal.ONE;


  @Before
  public void setUp()
  {
    fixture = new Fixture();
  }


  @Test
  public void testToExternalSuccess()
  {
    fixture.givenWeHaveAnInternalVehicleModel();
    fixture.whenWeCallToExternal();
    fixture.thenWeAssertVehicleResponseIsCorrect();
  }

  @Test
  public void testToExternalNullVehicle()
  {
    fixture.whenWeCallToExternal();
    fixture.thenWeAssertVehicleResponseIsEmptyObject();
  }

  private class Fixture
  {
    public static final String CODE = "code";
    public static final String MAKE = "make";
    public static final String MODEL = "model";
    public static final String DESC = "desc";

    private VehicleModelMapper vehicleModelMapper;

    private Vehicle vehicle;
    private VehicleResponse vehicleResponse;


    private Fixture()
    {
      vehicleModelMapper = new VehicleModelMapper();
    }

    private void givenWeHaveAnInternalVehicleModel()
    {
      vehicle = new Vehicle();
      vehicle.setCode(CODE);
      vehicle.setMake(MAKE);
      vehicle.setModel(MODEL);
      vehicle.setDesc(DESC);
      vehicle.setPrice(PRICE);
    }

    private void whenWeCallToExternal()
    {
      vehicleResponse = vehicleModelMapper.toExternal(vehicle);
    }

    private void thenWeAssertVehicleResponseIsCorrect()
    {
      assertEquals(CODE, vehicleResponse.getCode());
      assertEquals(MAKE, vehicleResponse.getMake());
      assertEquals(MODEL, vehicleResponse.getModel());
      assertEquals(DESC, vehicleResponse.getDesc());
      assertEquals(PRICE, vehicleResponse.getPrice());
    }

    public void thenWeAssertVehicleResponseIsEmptyObject() {
      assertNotNull(vehicleResponse);
    }
  }
}
