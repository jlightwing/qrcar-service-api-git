package com.lightwing.qrcar.service.api.mapper;

import com.lightwing.qrcar.da.api.model.Vehicle;
import com.lightwing.qrcar.jaxb.VehicleResponse;

import javax.inject.Named;

/**
 * Contains mappers to convert between the internal data objects, and the external jaxb generated objects
 */
@Named
public class VehicleModelMapper {

  /**
   * Maps a Vehicle object, to a VehicleResponse object
   * @param vehicleInternal the internal Vehicle object to map
   * @return the external VehicleResponse
   */
  public VehicleResponse toExternal(Vehicle vehicleInternal)
  {
    VehicleResponse vehicleResponseExternal = new VehicleResponse();

    if(vehicleInternal != null)
    {
      vehicleResponseExternal.setCode(vehicleInternal.getCode());
      vehicleResponseExternal.setMake(vehicleInternal.getMake());
      vehicleResponseExternal.setModel(vehicleInternal.getModel());
      vehicleResponseExternal.setDesc(vehicleInternal.getDesc());
      vehicleResponseExternal.setPrice(vehicleInternal.getPrice());
    }

    return vehicleResponseExternal;
  }
}

